import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import WindiCSS from 'vite-plugin-windicss';
const isProduction = process.env.NODE_ENV === 'production';

// https://vitejs.dev/config/
export default defineConfig({
  base: isProduction ? '/calculador-cuentas/' : '/',
  publicDir: isProduction ? 'public-vue' : 'public',
  plugins: [vue(), WindiCSS()],
});
