import { defineConfig } from 'vite-plugin-windicss';
import colors from 'windicss/colors';
import typography from 'windicss/plugin/typography';
import forms from 'windicss/plugin/forms';

export default defineConfig({
  darkMode: 'class',
  plugins: [typography, forms],
  theme: {
    extend: {
      colors: {
        teal: {
          100: colors.teal,
        },
      },
    },
  },
});
